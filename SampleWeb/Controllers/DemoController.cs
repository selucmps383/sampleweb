﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace SampleWeb.Controllers
{
    public class DemoController : Controller
    {
        public IActionResult Index()
        {
            // Silly example, don't do this normally please :)
            System.IO.File.WriteAllText(@".\..\demo.txt", "assignment completed");
            return Content("you did it :D", "text/plain");
        }
    }
}